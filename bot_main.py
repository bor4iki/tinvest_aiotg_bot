import logging
import aiogram.utils.markdown as md
from aiogram.types import ParseMode
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher import FSMContext

from config import TOKEN_TG, ID, BROKER, BROKER_IIS
from tinvest_main import GetCash, GetContent
from keyboards import keyboard

API_TOKEN = TOKEN_TG

# Configure logging
logging.basicConfig(level=logging.INFO)

# Initialize bot and dispatcher
bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)


class Form(StatesGroup):
    broker_name = State()
    broker = State()


@dp.message_handler(commands=['start'])
async def send_welcome(message: types.Message):
    """
    This handler will be called when user sends `/start` command
    """
    if message.chat.id != ID:
        logging.warning(f'{message.chat.id} - начал использовать бота')
    else:
        await message.answer("Буду напоминать только тебе, что у тебя происходит на биржевом рынке")


@dp.message_handler(commands=['help'])
async def help_message(message: types.Message):
    """
    This handler will be called when user sends `/help` command
    """
    if message.chat.id != ID:
        logging.warning(f'{message.chat.id} - попытался написать хелп')
    else:
        text = 'Здесь будут показан основной функционал бота.\n' \
               'Что он может, что нужно нажать.\n' \
               'Какие команды воспринимает.\n\n\n' \
               '/portfolio_today  -  просмотр стоимости портфеля на сегодня\n' \
               '/portfolio_content  -  просмотр состава портфеля'
        await message.answer(text)


@dp.message_handler(commands=['portfolio_today'])
async def today_message(message: types.Message):
    if message.chat.id != ID:
        pass
    else:
        await Form.broker_name.set()

        text = 'Выберите портфель, для показа его стоимости'
        await message.answer(text, reply_markup=keyboard)


@dp.message_handler(state=Form.broker_name)
async def message_cash(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['broker_name'] = message.text

        if message.text == 'Портфель':
            broker = BROKER
        else:
            broker = BROKER_IIS

        cash_tinvest = GetCash(broker_account_id=broker)
        cash = cash_tinvest.get_cost_portfolio()
        cash = str(cash).replace('.', ',')

        markup = types.ReplyKeyboardRemove()

        await message.answer(
            md.text(
                md.text('Состояние твоего портфеля на текущий момент\n'),
                md.text(md.bold(cash)),
                sep='\n',
            ),
            reply_markup=markup,
            parse_mode=ParseMode.MARKDOWN,
        )

        await state.finish()


@dp.message_handler(commands=['portfolio_content'])
async def content_message(message: types.Message):
    if message.chat.id != ID:
        pass
    else:
        await Form.broker.set()

        text = 'Выберите портфель, для показа его содержимого'
        await message.answer(text, reply_markup=keyboard)


@dp.message_handler(state=Form.broker)
async def message_content(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['broker'] = message.text

        if message.text == 'Портфель':
            broker = BROKER
        else:
            broker = BROKER_IIS

        contents = GetContent(broker_account_id=broker)

        content = contents.get_portfolio()

        markup = types.ReplyKeyboardRemove()

        await message.answer(
            md.text(
                md.text('Состав портфеля на текущий момент:', '\n'),
                md.text('Тикер', '|', 'Кол-во', '|', 'Цена', '|', 'Изменение цены'),
                md.text(md.hcode(content)),
                sep='\n',
            ),
            reply_markup=markup,
            parse_mode=ParseMode.HTML,
        )

        await state.finish()


@dp.message_handler()
async def echo(message: types.Message):
    # old style:
    # await bot.send_message(message.chat.id, message.text)

    # await message.answer(message.text)
    if message.chat.id != ID:
        logging.warning(f'{message.chat.id} - написал {message.text}')
    else:
        await message.answer('Нажимай /help')


if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)
