import tinvest
from pprint import pprint
from decimal import Decimal

from config import TI_TOKEN, BROKER, BROKER_IIS
from uttils import get_usd, localize, get_now

CLIENT = tinvest.SyncClient(TI_TOKEN)


def _get_user_account():
    api = tinvest.UserApi(CLIENT)
    response = api.accounts_get()
    if response.status_code == 200:
        pprint(response.parse_json().payload.accounts)


class GetCash:
    def __init__(self, broker_account_id):
        self.broker_account_id = broker_account_id
        self.api_portfolio = tinvest.PortfolioApi(CLIENT)
        self.api_operations = tinvest.OperationsApi(CLIENT)

    def get_free_money(self):
        request = self.api_portfolio.portfolio_currencies_get(broker_account_id=self.broker_account_id)

        moneys = request.parse_json().payload.currencies
        sum_cash = Decimal('0')
        for money in moneys:
            if money.currency == 'USD':
                sum_cash += Decimal(str(money.balance)) * Decimal(str(get_usd()))
            else:
                sum_cash += Decimal(str(money.balance))
        return sum_cash

    def get_portfolio_value(self):
        request = self.api_portfolio.portfolio_get(broker_account_id=self.broker_account_id)

        values = request.parse_json().payload.positions
        sum_cash = Decimal('0')
        for value in values:
            if value.average_position_price.currency == 'USD':
                sum_cash += Decimal(str(value.average_position_price.value)) \
                            * Decimal(str(get_usd())) \
                            * Decimal(str(value.balance)) \
                            + Decimal(str(value.expected_yield.value))
            else:
                sum_cash += Decimal(str(value.average_position_price.value)) \
                            * Decimal(str(value.balance)) \
                            + Decimal(str(value.expected_yield.value))
        return sum_cash

    def get_deposit_money(self):
        request = self.api_operations.operations_get(from_=localize(),
                                                     to=get_now(),
                                                     broker_account_id=self.broker_account_id)
        moneys = request.parse_json().payload.operations
        sum_cash = Decimal('0')
        for money in moneys:
            if money.operation_type == 'PayIn':
                if money.currency == 'USD':
                    sum_cash += Decimal(str(money.payment)) * Decimal(str(get_usd()))
                else:
                    sum_cash += Decimal(str(money.payment))

        return sum_cash

    def get_remove_money(self):
        request = self.api_operations.operations_get(from_=localize(),
                                                     to=get_now(),
                                                     broker_account_id=self.broker_account_id)
        moneys = request.parse_json().payload.operations
        sum_cash = Decimal('0')
        for money in moneys:
            if money.operation_type == 'PayOut':
                sum_cash += Decimal(str(money.payment))

        return sum_cash

    def get_cost_portfolio(self):
        money = self.get_portfolio_value() + self.get_free_money()
        money_deposit = self.get_deposit_money()
        money_removed = self.get_remove_money()
        profit_in_rub = money - (money_deposit - abs(money_removed))
        profit_in_percent = 100 * profit_in_rub / money_deposit

        mes = f'Пополнения портфеля: {round(money_deposit, 2)} руб.\n' \
              f'Сумма вывода денежных средств: {round(abs(money_removed), 2)} руб. \n' \
              f'Стоимость портфеля с вычетом выводов: {round(money_deposit - abs(money_removed), 2)} руб. \n' \
              f'Стоимость портфеля на текущий момент: {round(money, 2)} руб.\n' \
              f'Прибыль: {round(profit_in_rub, 2)} руб.\n' \
              f'Прибыль: {round(profit_in_percent, 2)} %'

        # print(mes)
        return mes
        # free_money = self.get_free_money()
        # portfolio_value = self.get_portfolio_value()
        #
        # portfolio_cost = free_money + portfolio_value
        # return round(portfolio_cost, 2)


class GetContent:
    def __init__(self, broker_account_id):
        self.broker_account_id = broker_account_id
        self.api_portfolio = tinvest.PortfolioApi(CLIENT)
        self.api_operations = tinvest.OperationsApi(CLIENT)

    def get_portfolio(self):
        request = self.api_portfolio.portfolio_get(broker_account_id=self.broker_account_id)

        contents = request.parse_json().payload.positions

        mes = ''
        for content in contents:
            mes += f'{content.ticker} | ' \
                   f'{content.balance} | ' \
                   f'{content.average_position_price.value} | ' \
                   f'{content.expected_yield.value} {content.expected_yield.currency}\n'

        return mes


portfolio = GetCash(BROKER_IIS)

if __name__ == '__main__':
    portfolio.get_cost_portfolio()
